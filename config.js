module.exports = {
    platform: 'gitlab',
    endpoint: process.env.CI_API_V4_URL,
    autodiscover: false,
    dryRun: null,
    gitAuthor: 'Renovate Bot <renovate-bot@example.com>',
    onboarding: true,
    onboardingConfig: {
        '$schema': 'https://docs.renovatebot.com/renovate-schema.json',
        extends: ['config:recommended'],
    },
    labels: ['renovate', 'dependencies', 'automated'],
    assignees: ['hendisantika'],
    baseDir: `${process.env.CI_PROJECT_DIR}/renovate`,
    optimizeForDisabled: true,
    repositoryCache: 'enabled',
    registryAliases: {
        '$CI_REGISTRY': process.env.CI_REGISTRY,
    },
    hostRules: [
        {
            hostType: 'docker',
            matchHost: 'https://index.docker.io',
            username: 'hendisantika',
            password: process.env.DOCKERHUB_TOKEN,
        },
        {
            matchHost: `https://${process.env.CI_REGISTRY}`,
            username: 'renovate-bot',
            password: process.env.RENOVATE_TOKEN,
        },
        {
            hostType: 'gradle',
            matchHost: process.env.CI_API_V4_URL,
            token: process.env.RENOVATE_TOKEN,
        },
    ],
}
