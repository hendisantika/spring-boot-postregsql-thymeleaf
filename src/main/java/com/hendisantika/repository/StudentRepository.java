package com.hendisantika.repository;

import com.hendisantika.domain.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-postregsql-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.39
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

}
