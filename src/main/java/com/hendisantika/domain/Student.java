package com.hendisantika.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

/**
 * Created by IntelliJ IDEA. Project : spring-boot-postregsql-thymeleaf User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 09/10/17 Time: 05.37 To change this
 * template use File | Settings | File Templates.
 */
@Entity
@Table
@Data
public class Student {

    @Transient
    static final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String rollNo;
    private String name;
    private LocalDate dateOfBirth;

    @Transient
    public String getDateOfBirthFormatted() {
        return df.format(dateOfBirth);
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}

