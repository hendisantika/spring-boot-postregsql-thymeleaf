package com.hendisantika.service;

import com.hendisantika.domain.Student;
import com.hendisantika.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-postregsql-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.41
 * To change this template use File | Settings | File Templates.
 */
@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public Iterable<Student> getStudents(){
        return studentRepository.findAll();
    }
}
