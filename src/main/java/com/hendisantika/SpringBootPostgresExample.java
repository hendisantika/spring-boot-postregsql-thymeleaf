package com.hendisantika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-postregsql-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/17
 * Time: 05.36
 * To change this template use File | Settings | File Templates.
 */
@SpringBootApplication
public class SpringBootPostgresExample {
    public static void main(String [] argv){
        SpringApplication.run(SpringBootPostgresExample.class, argv);
    }
}
